#!/bin/bash

dir=$1

for d in ${d}*/; do
  noDomains=$(cat ${d}/zfs_log | grep "Total number of domains" | grep -o -E '[0-9]+' | xargs echo)
  timesteps=$(cat ${d}/zfs_log | grep 'No. timesteps:' | tail -1 | head -1 | awk '{print $3}')
  runTime=$(cat ${d}/zfs_log | grep "Run time:" | tail -1 | head -1 | awk '{print $3}')
  echo "$noDomains $timesteps $runTime" >> scalability.tmp
done

sort -k 1n scalability.tmp > scalability.txt
rm scalability.tmp
