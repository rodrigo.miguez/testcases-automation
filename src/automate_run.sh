#!/bin/bash

##################################################################################################
# This script can be used to easily get the performance (scaling) of a specific test case in ZFS.
# It prepares the test cases and submit jobs according to the inputs provided by the user
# For more details refer to: automate_tc_runs/src/README.txt
##################################################################################################

set -u
set -e

# Read functions from external script file
source Functions.sh

# Define terminal colors
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

# Display/store data and time variables
Date=$(date +"%Y-%m-%d")
echo "Date: $Date"
Time=$(date +"%T")
echo "Current time: $Time"

# Get ZFS executable path
globalPath=~/
echo ""
read -p "Path of zfs executable: $globalPath" zfsPathTmp
zfsPath=${globalPath}${zfsPathTmp}
echo $zfsPath

# Get src/ path
srcDir=$(pwd)
echo ${srcDir}

# Get testcases/ path
cd ../testcases
tcDir=$(pwd)
echo ${tcDir}

# Get output/ path
cd ../output
outDir=$(pwd)
echo ${outDir}

# Display available test cases
echo -e "\nChoose one of the testcases available:"
ls -1 --color=auto ${tcDir}; echo ""
# Store test case name
read -p "Testcase name: " tcName

# Create working folder at output/
workDirName=${tcName}_out
if [[ -e $workDirName ]]; then
	echo -e "\nFolder already exists. Cleaning up old data..."
	source ${srcDir}/utils/cleanup.sh
fi
mkdir ${workDirName}
cd ${workDirName}
workDir=$(pwd)
echo -e "\nPath of work directory:"
echo ${workDir}

# Display available partitions
echo -e "\nChoose one of the partition types available:"
getPartitionNames; echo ""
# Store partition type
read -p "Partition type: " partitionType; echo ""

# Obtain partition details
maxNoNodes=$(getPartitionNodes $partitionType)
maxProcesses=$(getPartitionCPUs $partitionType)
maxCoresPerNode=$((${maxProcesses}/${maxNoNodes}))

# Store maximum job wall time and base number of time steps
read -p "Specify the maximum job walltime in hh:mm:ss format: " jobWalltime
read -p "Specify the base number of timesteps: " baseNoTimesteps

# Set number of processes at the beginning and end of the run
read -p "Initial number of processes: " initialNoProcesses
while [ true ]; do
  read -p "Final number of processes (max. number of processes is $maxProcesses): " finalNoProcesses
  if [[ $finalNoProcesses -le $maxProcesses ]]; then
    break
  fi
done

# Set number of cores to be user per node
ratio=$(awk -v a="$maxNoNodes" -v b="$finalNoProcesses" 'BEGIN {print a/b}')
minCoresPerNode=$(awk -v c="$ratio" 'BEGIN {x=1/c; printf("%.0f", (x == int(x)) ? x : int(x)+1)}')
while [ true ]; do
  read -p "Number of cores per node (available from  $minCoresPerNode to $maxCoresPerNode): " nCoresPerNode
  if [[ $nCoresPerNode -le $maxCoresPerNode && $nCoresPerNode -ge $minCoresPerNode ]]; then
    break
  fi
  echo -ne "${RED}"
  echo "Please select a number between the available range!"
  echo -ne "${NC}"
done

nNodes=1
tmpJobName=auto_np
incrValue=0
# Define the incrementation rule for the number of processes used
# avalilable options: Linear/Exponential
echo -e "\nChoose on of the available incrementation rule:"
echo -e "${GREEN}Linear\nExponential${NC}\n"
read -p "Incrementation rule: " incrRule
if [[ $incrRule == "Linear" ]]; then
  read -p "Select an incrementation value: " incrValue
else
  incrValue=i
fi

echo ""
# Choose whether to prepare and submit jobs or only prepare test cases
read -p "Do you want to prepare and submit all the test cases? [y/n]: " runJob

# Output log file (header)
writeOutputHeader $Date $Time $zfsPath $srcDir $tcDir $tcName $partitionType $jobWalltime\
						      $baseNoTimesteps $initialNoProcesses $finalNoProcesses $nCoresPerNode\
						      $incrRule $incrValue

# Loop to prepare test cases and submit jobs
for ((i=${initialNoProcesses}; i<=${finalNoProcesses}; i=i+${incrValue})); do
  processes=${i}
  jobName=${tmpJobName}${processes}
  testcaseName=${tcName}_noDomains${processes}_out.tmp

  if [[ ! -e $testcaseName ]]; then
    mkdir $testcaseName

    # Copy files and link ZFS in the template directory
    cp -r ${tcDir}/${tcName}/* ${testcaseName}
    ln -s $zfsPath/zfs  $testcaseName
    cp ${srcDir}/utils/jobscript ${testcaseName}

		# Calculate the current number time steps for the current run
    currentNoTimesteps=$(defTimesteps $baseNoTimesteps $initialNoProcesses $processes)

    replaceParameter $testcaseName/properties_run.toml "timeSteps" $currentNoTimesteps
    replaceParameter $testcaseName/properties_run.toml "noDomains" $processes
    replaceParameter $testcaseName/properties_run.toml "solutionInterval" $currentNoTimesteps
    replaceParameter $testcaseName/properties_run.toml "analysisInterval" $currentNoTimesteps

    replaceParameter $testcaseName/jobscript "noDomains" $processes
    replaceParameter $testcaseName/jobscript "--time" $jobWalltime
    replaceParameter $testcaseName/jobscript "--partition" $partitionType
    replaceParameter $testcaseName/jobscript "--job-name" $jobName

    # Identify the number of tasks/nodes that must be requested according to number of processes
    if [[ ${processes} -gt ${nCoresPerNode} ]]; then
      nNodes=$((${processes}/${nCoresPerNode}))
      if [[ $((${processes}%${nCoresPerNode})) -gt 0 ]]; then
        nNodes=$((${nNodes}+1))
      fi
    fi
    replaceParameter $testcaseName/jobscript "--ntasks" $nNodes
    replaceParameter $testcaseName/jobscript "--nodes" $nNodes

    # Output log file (run)
		writeOutputRun $jobName $processes $currentNoTimesteps $nNodes

    cd ${testcaseName}
    # Switch between:
		# y: prepare test cases and submit jobs
		# n: prepare the test cases only
		case "${runJob}" in
			y)
	      echo -e "\nRunning job..."
				sbatch jobscript
				;;
			n)
	      echo -e "\nJob not submitted..."
				;;
		esac
    cd ${workDir}

  else
    echo $testcaseName" already exists."
  fi
done

# Create symbolic link to data extraction file
ln -s ${srcDir}/utils/extractData.sh

echo -e "\ndone!"
