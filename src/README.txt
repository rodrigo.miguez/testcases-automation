The current version of the automation_run script only works on the AIA cluster.

How to:
Step 1 - Copy the test case(s) to automate_tc_runs/testcases/
Step 2 - Generate the grid file; before running the script the grid should be at out/, inside the
         test case directory
Step 3 - Run the script located at src/ using: ./automate_run.sh
Step 4 (Optional) - Run the script located at automate_tc_runs/output/<testcaseName>_out/ using:
										./extractData.sh
