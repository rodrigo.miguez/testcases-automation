#!/bin/bash

#############################################################
#        Funciton file for automate_run.sh script
#############################################################

#getPartitionDetails() {
#	if [ "$#" -ne 1 ]; then
#		echo "convTime needs 1 argument!" >&2
#		exit 2
#	fi
#	partition_name=$1
#
#	scontrol show partition | grep "PartitionName" | sed 's/^PartitionName=//'
#}


# Get partition names (slurm)
getPartitionNames() {
  GREEN='\033[0;32m'
  NC='\033[0m'

	echo -ne "${GREEN}"
	scontrol show partition | grep "PartitionName" | sed 's/^PartitionName=//'
	echo -ne "${NC}"
}


# Get number of nodes of a partition (slurm)
# parameter [in]: partition name
getPartitionNodes() {
  if [ "$#" -ne 1 ]; then
    echo "convTime needs 1 argument!" >&2
    exit 2
  fi
  partitionName=$1

  TotalNodes=$(scontrol show partition=$partitionName | grep "TotalNodes" | \
                                            sed -nr '/TotalNodes=/ s/.*TotalNodes=([^ ]+).*/\1/p')
  echo $TotalNodes
}


# Get number of cpus of a partition (slurm)
# parameter [in]: partition name
getPartitionCPUs() {
  if [ "$#" -ne 1 ]; then
    echo "convTime needs 1 argument!" >&2
    exit 2
  fi
  partitionName=$1

  TotalCPUs=$(scontrol show partition=$partitionName | grep "TotalCPUs" | \
                                            sed -nr '/TotalCPUs=/ s/.*TotalCPUs=([^ ]+).*/\1/p')
  echo $TotalCPUs
}


# Get the number of processes and initial timestep/walltime and return a new
# updated values according to the number of processes
# parameter [in]: initial number of timesteps, initial number of processes
#									current number of processes
defTimesteps () {
  if [ "$#" -ne 3 ]; then
    echo "defTimesteps needs 3 arguments!" >&2
      exit 2
  fi
  baseNoTimesteps=$1
  initialNoProcesses=$2
  currentNoProcesses=$3

  # Here: Calculate order of magnitude
  order=1
  n=$((${currentNoProcesses}/${initialNoProcesses}))
  while [ true ]; do
    rem=$((${n}%2))
    n=$((${n}/2))
    if [[ $n -eq 0 ]]; then
      break
    fi
    order=$((${order}*2))
  done
  currentNoTimesteps=$((${baseNoTimesteps}*${order}))
  echo $currentNoTimesteps
}


# Replace the value of a parameter in the given property file.
# parameter [in]: filename, parameter name, new parameter value
replaceParameter () {
  if [ "$#" -ne 3 ]; then
    echo "replaceParameter needs 3 arguments!" >&2
      exit 2
  fi
  file=$1
  param=$2
  val=$3

  sed_p="s/(${param}\s*=\s*).*/\1${val}/"
  sed -i -re "$sed_p" $file
}


# Write output file header
# parameter [in]: date, time, zfs path, src path, testcases path, testcase name, partition type,
#                 walltime, initial number of timesteps, initial number of processes,
#                 final number of processes, core per node, incrementation rule, increment per run
writeOutputHeader() {
  if [ "$#" -ne 14 ]; then
    echo "writeOutputHeader needs 14 arguments!" >&2
      exit 2
  fi

	header=$(cat <<-EOM
		Date: $1
		Current time: $2

		ZFS path: $3
		Script path: $4
		Testcases path: $5

		Testcase name: $6

		Partition type: $7

		Walltime: $8

		Initial number of timesteps: $9
		Initial number of processes: ${10}
		Final number of processes: ${11}
		Cores per node: ${12}

		Incrementation rule: ${13}
	EOM
	)

	if [[ ${13} == "Linear" ]]; then
		header=$(cat <<-EOM
			$header
			processes increment per run: ${14}
		EOM
		)
	fi

	header=$(cat <<-EOM
		$header

		Preparing test cases...

	EOM
	)

	echo "$header" > output.log
}


# Write output file run
# parameter [in]: job name, number of processes, number of timesteps, number of nodes,
writeOutputRun() {
  if [ "$#" -ne 4 ]; then
    echo "writeOutputRun needs 4 arguments!" >&2
      exit 2
  fi

	run=$(cat <<-EOM
		Job name: $1
		Number of processes: $2
		Number of timesteps: $3
		Number of nodes: $4
	EOM
	)

	echo -e "\n$run" >> output.log
}
